﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BE;

namespace DAL
{
    public static class MP_ESCALA
    {
        static ACCESO acceso = new ACCESO();

        internal static List<CIUDAD> EscalasVuelo(VUELO v)
        {
            List<CIUDAD> ciudades = new List<CIUDAD>();
            DataTable tabla = new DataTable();

            acceso.Abrir();
            tabla = acceso.Leer("ListarEscala");
            acceso.Cerrar();

            List<CIUDAD> ciudadestotal = MP_CIUDAD.ListarCiudades();

            foreach (DataRow r in tabla.Rows)
            {
                foreach (CIUDAD c in ciudadestotal)
                {
                    if (c.ID == int.Parse(r[1].ToString()) && v.NumeroVuelo == int.Parse(r[0].ToString()))
                    {
                        ciudades.Add(c);
                    }
                }
            }

            return ciudades;
        }

        public static void AltaEscalas(VUELO_INTERNACIONAL v)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            foreach (BE.CIUDAD c in v.Escala)
            {
                parametros.Add(acceso.CrearParametro("@Vuelo", v.NumeroVuelo)); //VUELO INTERNACIONAL
                parametros.Add(acceso.CrearParametro("@Ciudad", c.ID));

                acceso.Abrir();
                acceso.Escribir("AltaEscala", parametros);
                acceso.Cerrar();

                parametros.Clear();
            }    
        }

        public static void BajaEscala(VUELO_INTERNACIONAL v)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            
            parametros.Add(acceso.CrearParametro("@Vuelo", v.NumeroVuelo)); //VUELO INTERNACIONAL
            
            acceso.Abrir();
            acceso.Escribir("BajaEscala", parametros);
            acceso.Cerrar();

        }

    }
}

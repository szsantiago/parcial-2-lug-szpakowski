﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BE;

namespace DAL
{
    public class MP_PASAJERO
    {
        ACCESO acceso = new ACCESO();

        public void AltaPasajero(BE.PASAJERO p)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@Nombre", p.Nombre));
            parametros.Add(acceso.CrearParametro("@DNI", p.DNI));

            acceso.Abrir();
            acceso.Escribir("AltaPasajero", parametros);
            acceso.Cerrar();

        }

        public void ModificarPasajero(BE.PASAJERO p)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@Nombre", p.Nombre));
            parametros.Add(acceso.CrearParametro("@DNI", p.DNI));

            acceso.Abrir();
            acceso.Escribir("ModificarPasajero", parametros);
            acceso.Cerrar();
        }

        public void BajaPasajero(BE.PASAJERO p)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            
            parametros.Add(acceso.CrearParametro("@DNI", p.DNI));

            acceso.Abrir();
            acceso.Escribir("BajaPasajero", parametros);
            acceso.Cerrar();
        }

        public static List<BE.PASAJERO> ListarPasajeros()
        {
            ACCESO acceso = new ACCESO();
            List<BE.PASAJERO> pasajeros = new List<PASAJERO>();
            DataTable tabla = new DataTable();

            acceso.Abrir();
            tabla = acceso.Leer("ListarPasajeros");
            acceso.Cerrar();

            List<VUELO> vuelos = MP_VUELO.ListVuelos();
            

            foreach (DataRow r in tabla.Rows)
            {
                BE.PASAJERO p = new PASAJERO();

                p.DNI = int.Parse(r[0].ToString());
                p.Nombre = r[1].ToString();

                p.Vuelos = MP_VUELO_PASAJERO.VuelosPorPasajero(p);

                pasajeros.Add(p);
            }

            return pasajeros;
        }

    }
}

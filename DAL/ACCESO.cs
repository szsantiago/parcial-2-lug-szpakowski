﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    internal class ACCESO
    {
        private SqlConnection conexion;

        public void Abrir()
        {
            conexion = new SqlConnection(@"Data Source=.\SQLEXPRESS;Initial Catalog=AEROPUERTO;Integrated Security=SSPI");
            conexion.Open();
        }

        public void Cerrar()
        {
            conexion.Close();
            conexion.Dispose();
            GC.Collect();
        }

        public SqlCommand CrearComando(string SQL, List<SqlParameter> parametros = null, CommandType tipo = CommandType.StoredProcedure)
        {
            SqlCommand comando = new SqlCommand(SQL);

            comando.CommandType = tipo;

            if (parametros != null && parametros.Count > 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());
            }

            comando.Connection = conexion;
            return comando;
        }

        public DataTable Leer(string sql, List<SqlParameter> parametros = null)
        {
            SqlDataAdapter adaptador = new SqlDataAdapter();

            adaptador.SelectCommand = CrearComando(sql, parametros);

            DataTable tabla = new DataTable();

            adaptador.Fill(tabla);

            return tabla;
        }

        public int LeerEscalar(string sql)
        {
            return int.Parse(CrearComando(sql).ExecuteScalar().ToString());
        }


        public int Escribir(string sql, List<SqlParameter> parametros = null)
        {
            SqlCommand comando = CrearComando(sql, parametros, CommandType.StoredProcedure);

            int filasAfectadas = 0;

            try
            {
                filasAfectadas = comando.ExecuteNonQuery();
            }
            catch
            {
                filasAfectadas = -1;
            }

            return filasAfectadas;
        }

        public SqlParameter CrearParametro(string nombre, string valor)
        {
            SqlParameter parametro = new SqlParameter(nombre, valor);
            parametro.DbType = DbType.String;
            return parametro;
        }

        public SqlParameter CrearParametro(string nombre, DateTime valor)
        {
            SqlParameter parametro = new SqlParameter(nombre, valor);
            parametro.DbType = DbType.DateTime;
            return parametro;
        }

        public SqlParameter CrearParametro(string nombre, int valor)
        {
            SqlParameter parametro = new SqlParameter(nombre, valor);
            parametro.DbType = DbType.Int32;
            return parametro;
        }

        public SqlParameter CrearParametro(string nombre, double valor)
        {
            SqlParameter parametro = new SqlParameter(nombre, valor);
            parametro.SqlDbType = SqlDbType.Float;
            return parametro;
        }

        public SqlParameter CrearParametro(string nombre, int valor, bool v)
        {
            SqlParameter parametro = new SqlParameter(nombre, valor);
            parametro.SqlDbType = SqlDbType.Bit;
            return parametro;
        }
    }
}

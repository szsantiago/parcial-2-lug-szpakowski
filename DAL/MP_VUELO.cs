﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BE;

namespace DAL
{
    public class MP_VUELO
    {
        ACCESO acceso = new ACCESO();

        internal static List<VUELO> ListVuelos()
        {
            ACCESO acceso = new ACCESO();
            List<VUELO> vuelos = new List<VUELO>();
            DataTable tabla = new DataTable();

            acceso.Abrir();
            tabla = acceso.Leer("ListarVuelo");
            acceso.Cerrar();

            List<CIUDAD> ciudades = MP_CIUDAD.ListarCiudades();
            List<AVION> aviones = MP_AVION.ListarAviones();

            foreach (DataRow r in tabla.Rows)
            {
                VUELO v;
                if (bool.Parse(r[1].ToString()) == true)
                {
                    v = new VUELO();
                    v.Tipo = Tipo_Vuelo.NACIONAL;

                    foreach (CIUDAD c in ciudades)
                    {
                        if (c.ID == int.Parse(r[2].ToString()))
                        {
                            v.CIUDAD_DESTINO = c;
                        }
                        else if (c.ID == int.Parse(r[3].ToString()))
                        {
                            v.CIUDAD_ORIGEN = c;
                        }
                    }
                }
                else
                {
                    v = new VUELO_INTERNACIONAL();
                    v.Tipo = Tipo_Vuelo.INTERNACIONAL;

                    ((VUELO_INTERNACIONAL)v).Escala = MP_ESCALA.EscalasVuelo(v);

                    foreach (CIUDAD c in ciudades)
                    {
                        if (c.ID == int.Parse(r[2].ToString()))
                        {
                            v.CIUDAD_DESTINO = c;
                        }
                        else if (c.ID == int.Parse(r[3].ToString()))
                        {
                            v.CIUDAD_ORIGEN = c;
                        }
                    }

                    ((VUELO_INTERNACIONAL)v).PAIS_DESTINO = v.CIUDAD_DESTINO.PAIS;
                }

                

                v.NumeroVuelo = int.Parse(r[0].ToString());
                
                foreach (AVION a in aviones)
                {
                    if (a.ID == int.Parse(r[4].ToString()))
                    {
                        v.AVION = a;
                    }
                }

                v.FyH_Partida = DateTime.Parse(r[5].ToString());

                v.FyH_Llegada = DateTime.Parse(r[6].ToString());

                int es = int.Parse(r[7].ToString());
                switch (es)
                {
                    case 1: v.Estado = Estado.ESPERANDO; break;
                    case 2: v.Estado = Estado.COMPLETADO; break;
                    case 3: v.Estado = Estado.CANCELADO; break;
                    default: v.Estado = Estado.VOLANDO; break;
                }

                

                vuelos.Add(v);
            }

            return vuelos;
        }

        public static List<VUELO> ListarVuelos()
        {
            List<VUELO> vuelos = ListVuelos();

            List<PASAJERO> pasajeros = MP_PASAJERO.ListarPasajeros();

            //CADA PASAJERO, EN CADA VUELO EN EL QUE ESTUVO, RECORRO TODOS LOS VUELOS 
            //Y SI EL ID DEL VUELO DEL PASAJERO COINCIDE CON EL ID DE ALGUN VUELO, LO AGREGO

            foreach (PASAJERO p in pasajeros)       
            {
                foreach (VUELO vp in p.Vuelos)
                {
                    foreach (VUELO vuel in vuelos)
                    {
                        if (vp.NumeroVuelo == vuel.NumeroVuelo)
                        {
                            vuel.Pasajeros.Add(p);
                        }
                    }
                }
            }

            return vuelos;


        }

        public void AltaVueloNacional(BE.VUELO v)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@Tipo", 1)); //VUELO NACIONAL
            parametros.Add(acceso.CrearParametro("@CuidadDestino", v.CIUDAD_DESTINO.ID));
            parametros.Add(acceso.CrearParametro("@CiudadOrigen", v.CIUDAD_ORIGEN.ID));
            parametros.Add(acceso.CrearParametro("@Avion", v.AVION.ID));
            parametros.Add(acceso.CrearParametro("@Partida", v.FyH_Partida.ToString()));
            parametros.Add(acceso.CrearParametro("@Llegada", v.FyH_Llegada.ToString()));
            parametros.Add(acceso.CrearParametro("@Estado", 1));

            acceso.Abrir();
            acceso.Escribir("AltaVuelo", parametros);
            acceso.Cerrar();
        }

        public void AltaVueloInteracional(BE.VUELO v)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@Tipo", 2)); //VUELO INTERNACIONAL
            parametros.Add(acceso.CrearParametro("@CuidadDestino", v.CIUDAD_DESTINO.ID));
            parametros.Add(acceso.CrearParametro("@CiudadOrigen", v.CIUDAD_ORIGEN.ID));
            parametros.Add(acceso.CrearParametro("@Avion", v.AVION.ID));
            parametros.Add(acceso.CrearParametro("@Partida", v.FyH_Partida.ToString()));
            parametros.Add(acceso.CrearParametro("@Llegada", v.FyH_Llegada.ToString()));
            parametros.Add(acceso.CrearParametro("@Estado", 1));

            acceso.Abrir();
            acceso.Escribir("AltaVuelo", parametros);
            acceso.Cerrar();
        }

        public void BajaVuelo(BE.VUELO v)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@ID", v.NumeroVuelo));

            acceso.Abrir();
            acceso.Escribir("BajaVuelo", parametros);
            acceso.Cerrar();
        }

        public void Iniciar(BE.VUELO v)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            
            parametros.Add(acceso.CrearParametro("@ID", v.NumeroVuelo));
            parametros.Add(acceso.CrearParametro("@Estado", 4));
            

            acceso.Abrir();
            acceso.Escribir("ModificarVuelo", parametros);
            acceso.Cerrar();
        }

        public void Finalizar(BE.VUELO v)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            
            parametros.Add(acceso.CrearParametro("@ID", v.NumeroVuelo));
            parametros.Add(acceso.CrearParametro("@Estado", 2));


            acceso.Abrir();
            acceso.Escribir("ModificarVuelo", parametros);
            acceso.Cerrar();
        }

    }
}

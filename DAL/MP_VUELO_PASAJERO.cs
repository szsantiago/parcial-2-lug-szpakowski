﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BE;

namespace DAL
{
    static public class MP_VUELO_PASAJERO
    {
        static ACCESO acceso = new ACCESO();

        public static List<BE.VUELO> VuelosPorPasajero(BE.PASAJERO p)
        {
            List<BE.VUELO> vuelos = new List<VUELO>();
            DataTable tabla = new DataTable();

            acceso.Abrir();
            tabla = acceso.Leer("ListarVueloPasajeros");
            acceso.Cerrar();

            List<VUELO> vuelostotal = MP_VUELO.ListVuelos();
            
            foreach (DataRow r in tabla.Rows)
            {
                foreach (VUELO v in vuelostotal)
                {
                    if (int.Parse(r[0].ToString()) == v.NumeroVuelo && int.Parse(r[1].ToString()) == p.DNI)
                    {
                        vuelos.Add(v);
                    }
                }
            }

            return vuelos;
        }

        public static List<PASAJERO> PasajerosPorVuelo(VUELO v)
        {
            List<PASAJERO> pasajeros = new List<PASAJERO>();
            DataTable tabla = new DataTable();

            acceso.Abrir();
            tabla = acceso.Leer("ListarVueloPasajeros");
            acceso.Cerrar();

            List<PASAJERO> pasajerostotal = MP_PASAJERO.ListarPasajeros();

            foreach (DataRow r in tabla.Rows)
            {
                foreach (PASAJERO p in pasajerostotal)
                {
                    if (int.Parse(r[0].ToString()) == v.NumeroVuelo && int.Parse(r[1].ToString()) == p.DNI)
                    {
                        pasajeros.Add(p);
                    }
                }
            }

            return pasajeros;
        }

        public static void AgregarPasajero(BE.PASAJERO p, BE.VUELO v)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@Vuelo", v.NumeroVuelo));
            parametros.Add(acceso.CrearParametro("@Pasajero", p.DNI));

            acceso.Abrir();
            acceso.Escribir("AltaVueloPasajero", parametros);
            acceso.Cerrar();
        }

        public static void BajaVueloPasajeros(BE.VUELO v)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@ID", v.NumeroVuelo)); //VUELO INTERNACIONAL

            acceso.Abrir();
            acceso.Escribir("BajaVueloPasajero", parametros);
            acceso.Cerrar();
        }
    }
}

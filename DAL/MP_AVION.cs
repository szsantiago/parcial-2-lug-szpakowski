﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BE;

namespace DAL
{
    public class MP_AVION
    {
        ACCESO acceso = new ACCESO();

        public static List<AVION> ListarAviones()
        {
            ACCESO acceso = new ACCESO();
            List<BE.AVION> aviones = new List<AVION>();
            DataTable tabla = new DataTable();

            acceso.Abrir();
            tabla = acceso.Leer("ListarAvion");
            acceso.Cerrar();

            foreach (DataRow r in tabla.Rows)
            {
                BE.AVION a = new AVION();

                a.ID = int.Parse(r[0].ToString());
                a.Capacidad = int.Parse(r[1].ToString());
                int es = int.Parse(r[2].ToString());
                switch (es)
                {
                    case 1: a.Estado = Estado.ESPERANDO; break;
                    case 2: a.Estado = Estado.COMPLETADO; break;
                    case 3: a.Estado = Estado.CANCELADO; break;
                    case 4: a.Estado = Estado.VOLANDO; break;
                    default: a.Estado = Estado.LIBRE; break;
                }

                aviones.Add(a);
            }

            return aviones;
        }

        public void AltaAvion(BE.AVION a)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@Capacidad", a.Capacidad));
            parametros.Add(acceso.CrearParametro("@Estado", 5));

            acceso.Abrir();
            acceso.Escribir("AltaAvion", parametros);
            acceso.Cerrar();
        }

        public void ModificarCapacidad(BE.AVION a)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@ID", a.ID));
            parametros.Add(acceso.CrearParametro("@Capacidad", a.Capacidad));
            parametros.Add(acceso.CrearParametro("@Estado", 5));

            acceso.Abrir();
            acceso.Escribir("ModificarAvion", parametros);
            acceso.Cerrar();
        }

        public void ModificarEstado(BE.AVION a, BE.Estado est)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@ID", a.ID));
            parametros.Add(acceso.CrearParametro("@Capacidad", a.Capacidad));

            if (est == BE.Estado.ESPERANDO)
            {
                parametros.Add(acceso.CrearParametro("@Estado", 1));
            }
            else if (est == BE.Estado.LIBRE)
            {
                parametros.Add(acceso.CrearParametro("@Estado", 5));
            }
            else
            {
                parametros.Add(acceso.CrearParametro("@Estado", 4));
            }

            acceso.Abrir();
            acceso.Escribir("ModificarAvion", parametros);
            acceso.Cerrar();
        }

        public void BajaAvion(BE.AVION a)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@ID", a.ID));

            acceso.Abrir();
            acceso.Escribir("BajaAvion", parametros);
            acceso.Cerrar();
        }

        public void Despegar (BE.AVION a)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            int est;

            if (a.Estado == Estado.LIBRE)
            {
                est = 5;
            }
            else if (a.Estado == Estado.VOLANDO)
            {
                est = 4;
            }
            else 
            {
                est = 1;
            }
            parametros.Add(acceso.CrearParametro("@ID", a.ID));
            parametros.Add(acceso.CrearParametro("@Capacidad", a.Capacidad));
            parametros.Add(acceso.CrearParametro("@Estado", est));

            acceso.Abrir();
            acceso.Escribir("ModificarAvion", parametros);
            acceso.Cerrar();
        }

        public void Aterrizar(BE.AVION a)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            
            parametros.Add(acceso.CrearParametro("@ID", a.ID));
            parametros.Add(acceso.CrearParametro("@Capacidad", a.Capacidad));
            parametros.Add(acceso.CrearParametro("@Estado", 5));

            acceso.Abrir();
            acceso.Escribir("ModificarAvion", parametros);
            acceso.Cerrar();
        }

    }
}

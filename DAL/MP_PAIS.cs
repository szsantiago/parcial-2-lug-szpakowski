﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BE;

namespace DAL
{
    public class MP_PAIS
    {
        public static List<PAIS> ListarPaises()
        {
            ACCESO acceso = new ACCESO();
            List<PAIS> paises = new List<PAIS>();
            DataTable tabla = new DataTable();

            acceso.Abrir();
            tabla = acceso.Leer("ListarPais");
            acceso.Cerrar();

            foreach (DataRow r in tabla.Rows)
            {
                PAIS p = new PAIS();
                p.ID = int.Parse(r[0].ToString());
                p.Nombre = r[1].ToString();

                paises.Add(p);
            }

            return paises;
        }
    }
}

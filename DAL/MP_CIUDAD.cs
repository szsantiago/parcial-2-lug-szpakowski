﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BE;

namespace DAL
{
    public class MP_CIUDAD
    {
        ACCESO acceso = new ACCESO();
        
        public static List<BE.CIUDAD> ListarCiudades()
        {
            ACCESO acceso = new ACCESO();
            List<BE.CIUDAD> ciudades = new List<CIUDAD>();
            DataTable tabla = new DataTable();

            acceso.Abrir();
            tabla = acceso.Leer("ListarCiudad");
            acceso.Cerrar();

            List<PAIS> paises = MP_PAIS.ListarPaises();

            foreach (DataRow r in tabla.Rows)
            {
                CIUDAD c = new CIUDAD();
                c.ID = int.Parse(r[0].ToString());
                c.Nombre = r[1].ToString();
                c.Latitud = float.Parse(r[2].ToString());
                c.Longitud = float.Parse(r[3].ToString());

                foreach (PAIS p in paises)
                {
                    if (p.ID == int.Parse(r[4].ToString()))
                    {
                        c.PAIS = p;
                    }
                }
                
                ciudades.Add(c);
            }

            return ciudades;
        }

        public void AltaCiudad(BE.CIUDAD c)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@Nombre", c.Nombre));
            parametros.Add(acceso.CrearParametro("@Latitud", c.Latitud));
            parametros.Add(acceso.CrearParametro("@Longitud", c.Longitud));
            parametros.Add(acceso.CrearParametro("@Pais", c.PAIS.ID));

            acceso.Abrir();
            acceso.Escribir("AltaDestino", parametros);
            acceso.Cerrar();
        }

        public void ModificarCiudad(BE.CIUDAD c)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@ID", c.ID));
            parametros.Add(acceso.CrearParametro("@Nombre", c.Nombre));
            parametros.Add(acceso.CrearParametro("@Latitud", c.Latitud));
            parametros.Add(acceso.CrearParametro("@Longitud", c.Longitud));
            parametros.Add(acceso.CrearParametro("@Pais", c.PAIS.ID));

            acceso.Abrir();
            acceso.Escribir("ModificarCiudad", parametros);
            acceso.Cerrar();
        }

        public void BajaCiudad(BE.CIUDAD c)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@ID", c.ID));

            acceso.Abrir();
            acceso.Escribir("BajaCiudad", parametros);
            acceso.Cerrar();
        }
    }
}

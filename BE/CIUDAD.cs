﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class CIUDAD
    {

        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private PAIS pais;

        public PAIS PAIS
        {
            get { return pais; }
            set { pais = value; }
        }

        private double latitud;

        public double Latitud
        {
            get { return latitud; }
            set { latitud = value; }
        }

        private double longitud;

        public double Longitud
        {
            get { return longitud; }
            set { longitud = value; }
        }

        public override string ToString()
        {
            return Nombre;
        }

    }
}
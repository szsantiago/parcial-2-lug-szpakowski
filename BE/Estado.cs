﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public enum Estado
    {
        ESPERANDO,
        COMPLETADO,
        CANCELADO,
        VOLANDO,
        LIBRE
    }
}
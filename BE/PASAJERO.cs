﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class PASAJERO
    {

        private int dni;

        public int DNI
        {
            get { return dni; }
            set { dni = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private List<VUELO> vuelos;

        public List<VUELO> Vuelos
        {
            get { return vuelos; }
            set { vuelos = value; }
        }

        public override string ToString()
        {
            return Nombre;
        }

    }
}
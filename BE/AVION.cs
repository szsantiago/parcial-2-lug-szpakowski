﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class AVION
    {

        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }


        private int capacidad;

        public int Capacidad
        {
            get { return capacidad; }
            set { capacidad = value; }
        }

        private Estado estado;

        public Estado Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        public override string ToString()
        {
            return ID.ToString() + " / Cap: " + capacidad.ToString();
        }

    }
}
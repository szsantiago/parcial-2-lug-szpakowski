﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class VUELO_INTERNACIONAL : VUELO
    {

        private List<CIUDAD> escala = new List<CIUDAD>();

        public List<CIUDAD> Escala
        {
            get { return escala; }
            set { escala = value; }
        }

        private PAIS pais_destino;

        public PAIS PAIS_DESTINO
        {
            get { return pais_destino; }
            set { pais_destino = this.CIUDAD_DESTINO.PAIS; }
        }
    }
}
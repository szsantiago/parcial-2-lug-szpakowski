﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class VUELO
    {

        private int numvuelo;

        public int NumeroVuelo
        {
            get { return numvuelo; }
            set { numvuelo = value; }
        }

        private List<PASAJERO> pasajeros = new List<PASAJERO>();

        public List<PASAJERO> Pasajeros
        {
            get { return pasajeros; }
            set { pasajeros = value; }
        }

        private CIUDAD ciudad_origen;

        public CIUDAD CIUDAD_ORIGEN
        {
            get { return ciudad_origen; }
            set { ciudad_origen = value; }
        }

        private CIUDAD ciudad_destino;

        public CIUDAD CIUDAD_DESTINO
        {
            get { return ciudad_destino; }
            set { ciudad_destino = value; }
        }

        private Tipo_Vuelo tipo;

        public Tipo_Vuelo Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        private DateTime fyh_partida;

        public DateTime FyH_Partida
        {
            get { return fyh_partida; }
            set { fyh_partida = value; }
        }

        private DateTime fyh_llegada;   

        public DateTime FyH_Llegada
        {
            get { return fyh_llegada; }
            set { fyh_llegada = value; }
        }

        private AVION avion;

        public AVION AVION
        {
            get { return avion; }
            set { avion = value; }
        }

        private Estado estado;

        public Estado Estado
        {
            get { return estado; }
            set { estado = value; }
        }

    }
}
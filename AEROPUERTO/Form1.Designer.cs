﻿namespace AEROPUERTO
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mENUToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pASAJEROSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aVIONESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vUELOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cIUDADESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sALIRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lISTARVUELOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mENUToolStripMenuItem,
            this.sALIRToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(889, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mENUToolStripMenuItem
            // 
            this.mENUToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pASAJEROSToolStripMenuItem,
            this.aVIONESToolStripMenuItem,
            this.vUELOSToolStripMenuItem,
            this.cIUDADESToolStripMenuItem,
            this.lISTARVUELOSToolStripMenuItem});
            this.mENUToolStripMenuItem.Name = "mENUToolStripMenuItem";
            this.mENUToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.mENUToolStripMenuItem.Text = "MENU";
            // 
            // pASAJEROSToolStripMenuItem
            // 
            this.pASAJEROSToolStripMenuItem.Name = "pASAJEROSToolStripMenuItem";
            this.pASAJEROSToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.pASAJEROSToolStripMenuItem.Text = "PASAJEROS";
            this.pASAJEROSToolStripMenuItem.Click += new System.EventHandler(this.pASAJEROSToolStripMenuItem_Click);
            // 
            // aVIONESToolStripMenuItem
            // 
            this.aVIONESToolStripMenuItem.Name = "aVIONESToolStripMenuItem";
            this.aVIONESToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.aVIONESToolStripMenuItem.Text = "AVIONES";
            this.aVIONESToolStripMenuItem.Click += new System.EventHandler(this.aVIONESToolStripMenuItem_Click);
            // 
            // vUELOSToolStripMenuItem
            // 
            this.vUELOSToolStripMenuItem.Name = "vUELOSToolStripMenuItem";
            this.vUELOSToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.vUELOSToolStripMenuItem.Text = "VUELOS";
            this.vUELOSToolStripMenuItem.Click += new System.EventHandler(this.vUELOSToolStripMenuItem_Click);
            // 
            // cIUDADESToolStripMenuItem
            // 
            this.cIUDADESToolStripMenuItem.Name = "cIUDADESToolStripMenuItem";
            this.cIUDADESToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.cIUDADESToolStripMenuItem.Text = "CIUDADES";
            this.cIUDADESToolStripMenuItem.Click += new System.EventHandler(this.cIUDADESToolStripMenuItem_Click);
            // 
            // sALIRToolStripMenuItem
            // 
            this.sALIRToolStripMenuItem.Name = "sALIRToolStripMenuItem";
            this.sALIRToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.sALIRToolStripMenuItem.Text = "SALIR";
            this.sALIRToolStripMenuItem.Click += new System.EventHandler(this.sALIRToolStripMenuItem_Click);
            // 
            // lISTARVUELOSToolStripMenuItem
            // 
            this.lISTARVUELOSToolStripMenuItem.Name = "lISTARVUELOSToolStripMenuItem";
            this.lISTARVUELOSToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.lISTARVUELOSToolStripMenuItem.Text = "LISTAR VUELOS";
            this.lISTARVUELOSToolStripMenuItem.Click += new System.EventHandler(this.lISTARVUELOSToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 415);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "AEROPUERTO";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mENUToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pASAJEROSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aVIONESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vUELOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cIUDADESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sALIRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lISTARVUELOSToolStripMenuItem;
    }
}


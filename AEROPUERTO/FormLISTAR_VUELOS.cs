﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class FormLISTAR_VUELOS : Form
    {
        public FormLISTAR_VUELOS()
        {
            InitializeComponent();
        }

        private void FormLISTAR_VUELOS_Load(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void Actualizar()
        {
            comboBox1.DataSource = BLL.CIUDAD.ListarCiudades();
            comboBox2.Items.Add(BE.Tipo_Vuelo.INTERNACIONAL);
            comboBox2.Items.Add(BE.Tipo_Vuelo.NACIONAL);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<BE.VUELO> vuelos = new List<BE.VUELO>();

            List<BE.VUELO> vuel = BLL.VUELO.ListarVuelos();

            foreach (BE.VUELO v in vuel)
            {
                if (((BE.CIUDAD)comboBox1.SelectedItem) != null && v.CIUDAD_DESTINO.ID == ((BE.CIUDAD)comboBox1.SelectedItem).ID)
                {
                    if (comboBox2.SelectedItem != null && v.Tipo == (BE.Tipo_Vuelo)comboBox2.SelectedItem)
                    {
                        vuelos.Add(v);
                    }
                    else
                    {
                        MessageBox.Show("Elegir otro tipo de vuelo");
                    }
                }
            }

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = vuelos;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<BE.VUELO> vuelos = new List<BE.VUELO>();

            List<BE.VUELO> vuel = BLL.VUELO.ListarVuelos();

            foreach (BE.VUELO v in vuel)
            {
                if (v.Estado == BE.Estado.COMPLETADO)
                {
                    int totalPasAvion = v.AVION.Capacidad;
                    int totalPasVuel = v.Pasajeros.Count;
                    float porc;

                    porc = (totalPasVuel * 100) / totalPasAvion;

                    if (porc <= float.Parse(textBox1.Text))
                    {
                        vuelos.Add(v);
                    }
                }
                
            }

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = vuelos;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BE.VUELO vuelo = (BE.VUELO)dataGridView1.SelectedRows[0].DataBoundItem;

            listBox1.DataSource = null;
            listBox1.DataSource = vuelo.Pasajeros;
        }

        private void FormLISTAR_VUELOS_Enter(object sender, EventArgs e)
        {
            
        }
    }
}

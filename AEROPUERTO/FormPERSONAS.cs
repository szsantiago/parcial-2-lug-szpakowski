﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using BLL;

namespace AEROPUERTO
{
    public partial class FormPERSONAS : Form
    {
        BLL.PASAJERO pasajero = new BLL.PASAJERO();
        

        public FormPERSONAS()
        {
            InitializeComponent();
        }

        private void FormPERSONAS_Load(object sender, EventArgs e)
        {
            Actualizar();

            
        }

        private void Actualizar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = BLL.PASAJERO.ListaPasajeros();
            label1.Text = "";
            dataGridView2.DataSource = null;
            
        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BE.PASAJERO p = (BE.PASAJERO)dataGridView1.SelectedRows[0].DataBoundItem;

            label1.Text = p.Nombre + " - " + p.DNI.ToString();
            dataGridView2.DataSource = null;
            dataGridView2.DataSource = p.Vuelos;

            controlNuevoPasajero1.Pasajero = p;
            controlNuevoPasajero1.Modificar();

            

            comboBox1.DataSource = null;            
            comboBox1.DataSource = BLL.VUELO.ListarVuelos();
            comboBox1.DisplayMember = "CIUDAD_DESTINO";

            

        }

        private void controlNuevoPasajero1_ClickBoton(object sender, EventArgs e)
        {
            if (controlNuevoPasajero1.Pasajero != null)
            {
                pasajero.AltaPasajero(controlNuevoPasajero1.Pasajero);
                controlNuevoPasajero1.Pasajero = null;
                controlNuevoPasajero1.LimpiarTextBox();
                Actualizar();
            }
        }

        private void controlNuevoPasajero1_ClickBotonMod(object sender, EventArgs e)
        {
            if (controlNuevoPasajero1.Pasajero != null)
            {
                pasajero.ModificarPasajero(controlNuevoPasajero1.Pasajero);
                controlNuevoPasajero1.Pasajero = null;
                controlNuevoPasajero1.LimpiarTextBox();
                Actualizar();
            }
        }

        private void controlNuevoPasajero1_ClickBotonBaja(object sender, EventArgs e)
        {
            if (controlNuevoPasajero1.Pasajero != null)
            {
                pasajero.BajaPasajero(controlNuevoPasajero1.Pasajero);
                controlNuevoPasajero1.Pasajero = null;
                controlNuevoPasajero1.LimpiarTextBox();
                Actualizar();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (controlNuevoPasajero1.Pasajero != null)
            {
                BE.PASAJERO pasajero = controlNuevoPasajero1.Pasajero;
                
                BLL.VUELO vuelo = new BLL.VUELO();
                vuelo.AgregarPasajero(pasajero, (BE.VUELO)comboBox1.SelectedItem);

                Actualizar();
            }
        }
    }
}

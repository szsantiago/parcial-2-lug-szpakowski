﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class FormVUELOS : Form
    {
        BLL.VUELO vuelo = new BLL.VUELO();

        public FormVUELOS()
        {
            InitializeComponent();
        }

        private void FormVUELOS_Load(object sender, EventArgs e)
        {
            Actualizar();
            
        }

        private void Actualizar()
        {
            List<BE.VUELO> vuelosNacionales = new List<BE.VUELO>();
            List<BE.VUELO_INTERNACIONAL> vuelosInternacionales = new List<BE.VUELO_INTERNACIONAL>();

            foreach (BE.VUELO v in BLL.VUELO.ListarVuelos())
            {
                if (v is BE.VUELO_INTERNACIONAL)
                {
                    vuelosInternacionales.Add((BE.VUELO_INTERNACIONAL)v);
                }
                else
                {
                    vuelosNacionales.Add(v);
                }
            }

            dataGridView1.DataSource = vuelosNacionales;
            dataGridView2.DataSource = vuelosInternacionales;
        }


        private void controlVuelos1_ClickAlta(object sender, EventArgs e)
        {
            if (controlVuelos1.Vuelo != null)
            {
                
                vuelo.AltaVuelo(controlVuelos1.Vuelo);

                Actualizar();
            }
        }

        private void controlVuelos1_ClickBaja(object sender, EventArgs e)
        {
            if (controlVuelos1.Vuelo != null)
            {
                BE.VUELO v = controlVuelos1.Vuelo;

                if (v.Estado == BE.Estado.ESPERANDO)
                {
                    vuelo.BajaVuelo(v);
                }
                else
                {
                    MessageBox.Show("No se puede cancelar el vuelo, su estado es: " + v.Estado.ToString());
                }


                Actualizar();
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            controlVuelos1.Vuelo = (BE.VUELO)dataGridView1.SelectedRows[0].DataBoundItem;
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            controlVuelos1.Vuelo = (BE.VUELO)dataGridView2.SelectedRows[0].DataBoundItem;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (controlVuelos1.Vuelo != null)
            {
                vuelo.Despegue(controlVuelos1.Vuelo);

                Actualizar();
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (controlVuelos1.Vuelo != null)
            {
                if (controlVuelos1.Vuelo.Estado == BE.Estado.VOLANDO)
                {
                    vuelo.Aterriza(controlVuelos1.Vuelo);
                    Actualizar();
                }
                else
                {
                    MessageBox.Show("El todavia no despego");
                }
            }
        }

        private void FormVUELOS_Enter(object sender, EventArgs e)
        {
            Actualizar();
        }
    }
}

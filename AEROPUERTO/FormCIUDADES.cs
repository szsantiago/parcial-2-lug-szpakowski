﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using BLL;

namespace AEROPUERTO
{
    public partial class FormCIUDADES : Form
    {
        BLL.CIUDAD ciudad = new BLL.CIUDAD();
        BE.CIUDAD ciud = new BE.CIUDAD();  

        public FormCIUDADES()
        {
            InitializeComponent();
        }

        private void FormCIUDADES_Load(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void Actualizar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = BLL.CIUDAD.ListarCiudades();
        }

        private void controlNuevaCiudad1_ClickAltaCiudad(object sender, EventArgs e)
        {
            if (controlNuevaCiudad1.Ciudad != null)
            {
                bool ok = true;

                foreach (BE.CIUDAD c in BLL.CIUDAD.ListarCiudades())
                {
                    if (c.Nombre == controlNuevaCiudad1.Ciudad.Nombre)
                    {
                        ok = false;
                    }
                }

                if (ok)
                {
                    ciudad.AltaCiudad(controlNuevaCiudad1.Ciudad);
                    controlNuevaCiudad1.Limpiar();
                    Actualizar();
                }
                
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ciud = (BE.CIUDAD)dataGridView1.SelectedRows[0].DataBoundItem;
            controlNuevaCiudad1.Ciudad = ciud;
            controlNuevaCiudad1.LlenarDatos(ciud);
            
        }

        private void controlNuevaCiudad1_ClickModCiudad(object sender, EventArgs e)
        {
            if (controlNuevaCiudad1.Ciudad != null)
            {

                ciudad.ModificarCiudad(controlNuevaCiudad1.Ciudad);
                controlNuevaCiudad1.Limpiar();
                Actualizar();
            }
        }

        private void controlNuevaCiudad1_ClickBajaCiudad(object sender, EventArgs e)
        {
            if (controlNuevaCiudad1.Ciudad != null)
            {
                ciudad.BajaCiudad(controlNuevaCiudad1.Ciudad);
                controlNuevaCiudad1.Limpiar();
                Actualizar();
            }
        }
    }
}

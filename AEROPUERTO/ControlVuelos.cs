﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class ControlVuelos : UserControl
    {
        public ControlVuelos()
        {
            InitializeComponent();
        }

        private List<BE.CIUDAD> ciudadesOrigen;
        private List<BE.CIUDAD> ciudadesDestino;
        private List<BE.CIUDAD> ciudadesEscala = new List<BE.CIUDAD>();
        private BE.CIUDAD ciudadOrigen;
        private BE.CIUDAD ciudadDestino;

        private BE.VUELO vuelo;

        public BE.VUELO Vuelo
        {
            get { return vuelo; }
            set { vuelo = value; }
        }

        public event EventHandler ClickAlta;
        public event EventHandler ClickBaja;

        private void ControlVuelos_Load(object sender, EventArgs e)
        {
            ciudadesOrigen = BLL.CIUDAD.ListarCiudades();
            ciudadesDestino = BLL.CIUDAD.ListarCiudades();
            groupBox2.Visible = false;
            timer1.Enabled = true;
        }

        public void Limpiar()
        {
            
            comboBox1.DataSource = null;
            comboBox1.Items.Clear();
            comboBoxDestino.DataSource = null;
            comboBoxOrigen.DataSource = null;
            dateTimePickerSalida.Value = DateTime.Now;
            dateTimePickerLlegada.Value = DateTime.Now;
            listBox1.Items.Clear();
            groupBox2.Visible = false;
            ciudadesEscala.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if ((BE.CIUDAD)comboBoxOrigen.SelectedItem != null && (BE.CIUDAD)comboBoxDestino.SelectedItem != null && (BE.AVION)comboBox1.SelectedItem != null)
            {
                if (((BE.CIUDAD)comboBoxOrigen.SelectedItem).ID == ((BE.CIUDAD)comboBoxDestino.SelectedItem).ID)
                {
                    MessageBox.Show("No se puede hacer un vuelo a la misma ciudad de origen");
                }
                else
                {
                    if (((BE.CIUDAD)comboBoxOrigen.SelectedItem).PAIS.ID == ((BE.CIUDAD)comboBoxDestino.SelectedItem).PAIS.ID)
                    {
                        vuelo = new BE.VUELO();
                        vuelo.Tipo = BE.Tipo_Vuelo.NACIONAL;
                        vuelo.CIUDAD_DESTINO = (BE.CIUDAD)comboBoxDestino.SelectedItem;
                    }
                    else
                    {
                        foreach (BE.CIUDAD ciudes in listBox1.SelectedItems)
                        {
                            ciudadesEscala.Add(ciudes);
                        }

                        vuelo = new BE.VUELO_INTERNACIONAL();
                        vuelo.CIUDAD_DESTINO = (BE.CIUDAD)comboBoxDestino.SelectedItem;
                        ((BE.VUELO_INTERNACIONAL)vuelo).Escala = ciudadesEscala;
                        ((BE.VUELO_INTERNACIONAL)vuelo).PAIS_DESTINO = vuelo.CIUDAD_DESTINO.PAIS;
                        vuelo.Tipo = BE.Tipo_Vuelo.INTERNACIONAL;
                    }

                    vuelo.CIUDAD_ORIGEN = (BE.CIUDAD)comboBoxOrigen.SelectedItem;
                    vuelo.Estado = BE.Estado.ESPERANDO;
                    vuelo.FyH_Llegada = dateTimePickerLlegada.Value;
                    vuelo.FyH_Partida = dateTimePickerSalida.Value;
                    vuelo.AVION = (BE.AVION)comboBox1.SelectedItem;

                    BLL.AVION a = new BLL.AVION();
                    a.ModificarEstado(vuelo.AVION, BE.Estado.ESPERANDO);
                }

                Limpiar();

                ClickAlta(this, e);
            }
            else
            {
                MessageBox.Show("Completar campos");
            }
            
        }

        private void comboBoxOrigen_Click(object sender, EventArgs e)
        {
            comboBoxOrigen.DataSource = null;
            comboBoxOrigen.DataSource = ciudadesOrigen;
        }

        private void comboBoxDestino_Click(object sender, EventArgs e)
        {
            comboBoxDestino.DataSource = null;
            comboBoxDestino.DataSource = ciudadesDestino;
        }

        private void comboBox1_Click(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();

            List<BE.AVION> aviones = BLL.AVION.ListaAviones();

            foreach (BE.AVION a in aviones)
            {
                if (a.Estado == BE.Estado.LIBRE)
                {
                    comboBox1.Items.Add(a);
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (comboBoxOrigen.SelectedItem != null && comboBoxDestino.SelectedItem != null)
            {
                if (((BE.CIUDAD)comboBoxOrigen.SelectedItem).PAIS.ID != ((BE.CIUDAD)comboBoxDestino.SelectedItem).PAIS.ID)
                {
                    groupBox2.Visible = true;
                }
                else
                {
                    groupBox2.Visible = false;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BLL.AVION a = new BLL.AVION();
            a.ModificarEstado(vuelo.AVION, BE.Estado.LIBRE);

            ClickBaja(this, e);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ciudadDestino = (BE.CIUDAD)comboBoxDestino.SelectedItem;
            ciudadOrigen = (BE.CIUDAD)comboBoxOrigen.SelectedItem;

            foreach (BE.CIUDAD c in ciudadesDestino)
            {
                if (c.ID != ciudadDestino.ID && c.ID != ciudadOrigen.ID)
                {
                    ciudadesEscala.Add(c);
                }
            }

            listBox1.DataSource = null;
            listBox1.DataSource = ciudadesEscala;
            listBox1.DisplayMember = "Nombre";
            ciudadesEscala.Clear();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            listBox1.DataSource = null;
        }
    }
}

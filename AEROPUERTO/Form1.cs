﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using BLL;

namespace AEROPUERTO
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void sALIRToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pASAJEROSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormPERSONAS fp = new FormPERSONAS();
            fp.MdiParent = this;
            fp.Show();
        }

        private void aVIONESToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAVIONES fa = new FormAVIONES();
            fa.MdiParent = this;
            fa.Show();
        }

        private void cIUDADESToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormCIUDADES fc = new FormCIUDADES();
            fc.MdiParent = this;
            fc.Show();
        }

        private void vUELOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormVUELOS fv = new FormVUELOS();
            fv.MdiParent = this;
            fv.Show();
        }

        private void lISTARVUELOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormLISTAR_VUELOS flv = new FormLISTAR_VUELOS();
            flv.MdiParent = this;
            flv.Show();
        }
    }
}

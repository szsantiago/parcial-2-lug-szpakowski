﻿namespace AEROPUERTO
{
    partial class FormCIUDADES
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.controlNuevaCiudad1 = new AEROPUERTO.ControlNuevaCiudad();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(37, 303);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(638, 188);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // controlNuevaCiudad1
            // 
            this.controlNuevaCiudad1.Ciudad = null;
            this.controlNuevaCiudad1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.controlNuevaCiudad1.Location = new System.Drawing.Point(13, 24);
            this.controlNuevaCiudad1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.controlNuevaCiudad1.Name = "controlNuevaCiudad1";
            this.controlNuevaCiudad1.Size = new System.Drawing.Size(762, 219);
            this.controlNuevaCiudad1.TabIndex = 2;
            this.controlNuevaCiudad1.ClickAltaCiudad += new System.EventHandler(this.controlNuevaCiudad1_ClickAltaCiudad);
            this.controlNuevaCiudad1.ClickModCiudad += new System.EventHandler(this.controlNuevaCiudad1_ClickModCiudad);
            this.controlNuevaCiudad1.ClickBajaCiudad += new System.EventHandler(this.controlNuevaCiudad1_ClickBajaCiudad);
            // 
            // FormCIUDADES
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 559);
            this.Controls.Add(this.controlNuevaCiudad1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "FormCIUDADES";
            this.Text = "CIUDADES";
            this.Load += new System.EventHandler(this.FormCIUDADES_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private ControlNuevaCiudad controlNuevaCiudad1;
    }
}
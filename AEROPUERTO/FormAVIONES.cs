﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using BLL;
using System.Text.RegularExpressions;

namespace AEROPUERTO
{
    public partial class FormAVIONES : Form
    {
        BLL.AVION avion = new BLL.AVION();
        BE.AVION a = new BE.AVION();

        public FormAVIONES()
        {
            InitializeComponent();
        }

        private void FormAVIONES_Load(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void Actualizar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = BLL.AVION.ListaAviones();
            textBox1.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Regex expresion = new Regex(@"[0-9]");

            if (expresion.IsMatch(textBox1.Text))
            {
                BE.AVION a = new BE.AVION();

                
                a.Capacidad = int.Parse(textBox1.Text);
                a.Estado = Estado.LIBRE;

                avion.AltaAvion(a);

                Actualizar();
            }
            else
            {
                MessageBox.Show("Ingresar Capacidad");
            }

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            a = (BE.AVION)dataGridView1.SelectedRows[0].DataBoundItem;

            textBox1.Text = a.Capacidad.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (a.Estado == Estado.LIBRE)
            {
                a.Capacidad = int.Parse(textBox1.Text);

                avion.ModificarCapacidad(a);

                Actualizar();
            }
            else if (a.Estado == Estado.ESPERANDO)
            {
                MessageBox.Show("El avion esta asignado a un vuelo");
            }
            else
            {
                MessageBox.Show("El avion esta en vuelo");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (a != null)
            {
                if (a.Estado == Estado.LIBRE)
                {
                    avion.BajaAvion(a);

                    Actualizar();
                }
                else
                {
                    MessageBox.Show("El avion esta asignado a un vuelo");
                }
            }
            else
            {
                MessageBox.Show("Elegir avion");
            }
        }

        private void FormAVIONES_Enter(object sender, EventArgs e)
        {
            Actualizar();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using BLL;
using System.Text.RegularExpressions;

namespace AEROPUERTO
{
    public partial class ControlNuevoPasajero: UserControl
    {
        public ControlNuevoPasajero()
        {
            InitializeComponent();
        }

        public event EventHandler ClickBoton;
        public event EventHandler ClickBotonMod;
        public event EventHandler ClickBotonBaja;

        private BE.PASAJERO pasajero;

        public BE.PASAJERO Pasajero
        {
            get { return pasajero; }
            set { pasajero = value; }
        }

        private void ControlNuevoPasajero_Load(object sender, EventArgs e)
        {

        }

        public void LimpiarTextBox()
        {
            textBox1.Enabled = true;
            textBox1.Text = "";
            textBox2.Text = "";
        }

        public void Modificar()
        {
            textBox1.Enabled = false;
            textBox1.Text = pasajero.DNI.ToString();
            textBox2.Text = pasajero.Nombre;
        }

        private void button1_Click(object sender, EventArgs e)
        {            
            Regex expresion = new Regex(@"\A([0-9]{8})\Z");

            if (expresion.IsMatch(textBox1.Text))
            {
                List<BE.PASAJERO> pasajeros = BLL.PASAJERO.ListaPasajeros();
                bool ok = true;

                foreach (BE.PASAJERO p in pasajeros)
                {
                    if (p.DNI == int.Parse(textBox1.Text))
                    {
                        ok = false;
                    }
                }

                if (textBox2.Text != "" && ok)
                {
                    BE.PASAJERO p = new BE.PASAJERO();
                    p.DNI = int.Parse(textBox1.Text);
                    p.Nombre = textBox2.Text;

                    pasajero = p;
                }

                if (!ok)
                {
                    MessageBox.Show("DNI duplicado");
                }
                else if (textBox2.Text == "")
                {
                    MessageBox.Show("Ingresar nombre");
                }

              
            }
            else
            {
                MessageBox.Show("Error en el número de DNI");
            }

            if (this.ClickBoton != null)
            {
                ClickBoton(this, e);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (textBox2.Text != "" && textBox2.Text != pasajero.Nombre)
            {
                pasajero.Nombre = textBox2.Text;
            }

            if (this.ClickBotonMod != null)
            {
                ClickBotonMod(this, e);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            

            if (this.ClickBotonBaja != null)
            {
                ClickBotonBaja(this, e);
            }
        }
    }
}

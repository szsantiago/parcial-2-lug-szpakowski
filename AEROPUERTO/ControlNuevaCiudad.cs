﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using BE;
using BLL;
using System.Diagnostics;

namespace AEROPUERTO
{
    public partial class ControlNuevaCiudad : UserControl
    {
        List<BE.PAIS> paises;

        public ControlNuevaCiudad()
        {
            InitializeComponent();
        }

        public event EventHandler ClickAltaCiudad;
        public event EventHandler ClickModCiudad;
        public event EventHandler ClickBajaCiudad;

        private BE.CIUDAD ciudad;

        public BE.CIUDAD Ciudad
        {
            get { return ciudad; }
            set { ciudad = value; }
        }


        private void ControlNuevaCiudad_Load(object sender, EventArgs e)
        {
            paises = BLL.PAIS.ListaPaises();
        }

        private void comboBox1_Enter(object sender, EventArgs e)
        {
            comboBox1.DataSource = null;
            comboBox1.DataSource = paises;
        }

        public void Limpiar()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            comboBox1.DataSource = null;
            ciudad = null;
        }

        public void LlenarDatos(BE.CIUDAD c)
        {
            textBox1.Text = c.Nombre;
            textBox2.Text = c.Latitud.ToString();
            textBox3.Text = c.Longitud.ToString();
            comboBox1.DataSource = null;
            comboBox1.DataSource = paises;
            comboBox1.SelectedItem = paises[c.PAIS.ID-1];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //textBox2.Text.Replace('.', ',');
            //textBox3.Text.Replace('.', ',');

            double fValue;
            double fValue2;
            double.TryParse(textBox2.Text, NumberStyles.Float, CultureInfo.InvariantCulture, out fValue);
            double.TryParse(textBox3.Text, NumberStyles.Float, CultureInfo.InvariantCulture, out fValue2);

            if (fValue == 0 || fValue2 == 0)
            {
                MessageBox.Show("Error en las coordenadas, Latitud Máxima = 90, Mínima = -90");

                fValue = 100;
            }
            else
            {
                List<BE.CIUDAD> ciudades = BLL.CIUDAD.ListarCiudades();
                bool ok1 = true;

                if (textBox1.Text != "")
                {
                    foreach (BE.CIUDAD c in ciudades)
                    {
                        if (c.Nombre == textBox1.Text)
                        {
                            ok1 = false;
                        }
                    }

                    if (ok1)
                    {
                        if (fValue <= 90 && fValue >= -90)
                        {
                            if (fValue2 <= 180 && fValue2 >= -180)
                            {
                                
                                    bool ok = false;

                                    foreach (BE.PAIS p in paises)
                                    {
                                        if (p.Nombre == comboBox1.Text)
                                        {
                                            BE.CIUDAD c = new BE.CIUDAD();
                                            c.Nombre = textBox1.Text;
                                            c.Latitud = fValue;
                                            c.Longitud = fValue2;
                                            c.PAIS = p;
                                            ok = true;

                                            ciudad = c;
                                        }
                                    }

                                    if (!ok)
                                    {
                                        MessageBox.Show("Error en Pais");
                                    }
                                
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("La ciudad ya esta cargada en el sistema");
                    }
                }
                else
                {
                    MessageBox.Show("Ingresar nombre de la ciudad");
                }
            }
            
            if (ClickAltaCiudad != null)
            {
                ClickAltaCiudad(this, e);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (float.TryParse(textBox2.Text.Replace('.',','), out float r) == true && float.TryParse(textBox3.Text.Replace('.', ','), out float s) == true)
            {
                if (float.Parse(textBox2.Text.Replace('.', ',')) > 90 || float.Parse(textBox2.Text.Replace('.', ',')) < -90)
                {
                    MessageBox.Show("Latitud excede los limites");
                }
                else if (float.Parse(textBox3.Text.Replace('.', ',')) > 180 || float.Parse(textBox3.Text.Replace('.', ',')) < -180)
                {
                    MessageBox.Show("Longitud excede los limites");
                }
                else
                {
                    string lat = textBox2.Text.Replace(',', '.');
                    string lon = textBox3.Text.Replace(',', '.');

                    Process.Start("Chrome", "https://www.google.com.ar/maps/@" + lon + "," + lat + ",11z");
                }
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            string val1 = textBox2.Text.Replace(',', '.');
            string val2 = textBox3.Text.Replace(',', '.');
            
            double fValue;
            double fValue2;
            double.TryParse(val1, NumberStyles.Float, CultureInfo.InvariantCulture, out fValue);
            double.TryParse(val2, NumberStyles.Float, CultureInfo.InvariantCulture, out fValue2);

            if (fValue == 0 || fValue2 == 0)
            {
                MessageBox.Show("Error en las coordenadas, Latitud Máxima = 90, Mínima = -90");

                fValue = 100;
            }
            else
            {
                List<BE.CIUDAD> ciudades = BLL.CIUDAD.ListarCiudades();
                bool ok1 = true;

                if (textBox1.Text != "")
                {
                    foreach (BE.CIUDAD c in ciudades)
                    {
                        if (c.Nombre == textBox1.Text)
                        {
                            ok1 = false;
                        }
                    }

                    if (!ok1)
                    {
                        if (fValue <= 90 && fValue >= -90)
                        {
                            if (fValue2 <= 180 && fValue2 >= -180)
                            {

                                bool ok = false;

                                foreach (BE.PAIS p in paises)
                                {
                                    if (p.Nombre == comboBox1.Text)
                                    {
                                        BE.CIUDAD c = new BE.CIUDAD();
                                        c.ID = ciudad.ID;
                                        c.Nombre = textBox1.Text;
                                        c.Latitud = fValue;
                                        c.Longitud = fValue2;
                                        c.PAIS = p;
                                        ok = true;

                                        ciudad = c;
                                    }
                                }

                                if (!ok)
                                {
                                    MessageBox.Show("Error en Pais");
                                }

                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("La ciudad ya esta cargada en el sistema");
                    }
                }
                else
                {
                    MessageBox.Show("Ingresar nombre de la ciudad");
                }
            }

            if (ClickModCiudad != null)
            {
                ClickModCiudad(this, e);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (ciudad != null)
            {
                List<BE.VUELO> vuelos = BLL.VUELO.ListarVuelos();
                bool ok = true;

                foreach (BE.VUELO v in vuelos)
                {
                    if (v.CIUDAD_ORIGEN.ID == ciudad.ID || v.CIUDAD_DESTINO.ID == ciudad.ID)
                    {
                        ok = false;
                    }
                }

                if (ok)
                {
                    if (ClickBajaCiudad != null)
                    {
                        ClickBajaCiudad(this, e);
                    }
                }
                else
                {
                    MessageBox.Show("No se puede dar de baja a la ciudad porque posee vuelos");
                }

            }

            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using DAL;

namespace BLL
{
    public class CIUDAD
    {
        MP_CIUDAD ciudad = new MP_CIUDAD();

        public static List<BE.CIUDAD> ListarCiudades()
        {
            return MP_CIUDAD.ListarCiudades();
        }

        public void AltaCiudad(BE.CIUDAD c)
        {
            ciudad.AltaCiudad(c);
        }

        public void ModificarCiudad(BE.CIUDAD c)
        {
            ciudad.ModificarCiudad(c);
        }

        public void BajaCiudad(BE.CIUDAD c)
        {
            ciudad.BajaCiudad(c);
        }
    }
}

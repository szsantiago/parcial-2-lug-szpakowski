﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using DAL;

namespace BLL
{
    public class AVION
    {
        MP_AVION avion = new MP_AVION();

        public static List<BE.AVION> ListaAviones()
        {
            return MP_AVION.ListarAviones();
        }

        public void AltaAvion(BE.AVION a)
        {
            avion.AltaAvion(a);
        }

        public void ModificarCapacidad(BE.AVION a)
        {
            avion.ModificarCapacidad(a);
        }

        public void ModificarEstado(BE.AVION a, BE.Estado est)
        {
            avion.ModificarEstado(a, est);
        }

        public void BajaAvion(BE.AVION a)
        {
            avion.BajaAvion(a);
        }

        public void Despegar(BE.AVION a)
        {
            a.Estado = Estado.VOLANDO;

            avion.Despegar(a);
        }

        public void Aterrizar(BE.AVION a)
        {
            a.Estado = Estado.LIBRE;

            avion.Aterrizar(a);
        }
    }
}

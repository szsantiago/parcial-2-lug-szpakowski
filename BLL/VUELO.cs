﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE;
using DAL;

namespace BLL
{
    public class VUELO
    {
        MP_VUELO vuelo = new MP_VUELO();

        static public List<BE.VUELO> ListarVuelos()
        {
            return MP_VUELO.ListarVuelos();
        }

        public void AltaVuelo(BE.VUELO v)
        {
            if (v.Tipo == Tipo_Vuelo.NACIONAL)
            {
                vuelo.AltaVueloNacional(v);
            }
            else
            {
                vuelo.AltaVueloInteracional(v);

                List<BE.VUELO> vuelos = ListarVuelos();

                BE.VUELO vuel = (from BE.VUELO vue1 in vuelos
                                 orderby vue1.NumeroVuelo ascending
                                 select vue1
                                 ).LastOrDefault();

                if (vuel == null)
                {
                    v.NumeroVuelo = 0;
                }
                else
                {
                    v.NumeroVuelo = vuel.NumeroVuelo;
                }

                MP_ESCALA.AltaEscalas((BE.VUELO_INTERNACIONAL)v);
            }
            
        }

        public void AgregarPasajero(BE.PASAJERO p, BE.VUELO v)
        {
            MP_VUELO_PASAJERO.AgregarPasajero(p, v);
        }

        public void BajaVuelo(BE.VUELO v)
        {
            MP_VUELO_PASAJERO.BajaVueloPasajeros(v);

            if (v.Tipo == BE.Tipo_Vuelo.NACIONAL)
            {
                vuelo.BajaVuelo(v);
            }
            else
            {
                MP_ESCALA.BajaEscala((BE.VUELO_INTERNACIONAL)v);
                vuelo.BajaVuelo(v);
            }

            
        }

        public void Despegue(BE.VUELO v)
        {
            v.Estado = Estado.VOLANDO;
            BLL.AVION a = new BLL.AVION();
            a.Despegar(v.AVION);

            vuelo.Iniciar(v);
        }

        public void Aterriza(BE.VUELO v)
        {
            v.Estado = Estado.COMPLETADO;

            BLL.AVION a = new BLL.AVION();
            a.Aterrizar(v.AVION);

            vuelo.Finalizar(v);
        }
    }
}
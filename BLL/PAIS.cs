﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using DAL;

namespace BLL
{
    public class PAIS
    {

        public static List<BE.PAIS> ListaPaises()
        {
            return MP_PAIS.ListarPaises();
        }

    }
}

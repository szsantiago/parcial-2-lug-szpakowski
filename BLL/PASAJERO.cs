﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using DAL;

namespace BLL
{
    public class PASAJERO
    {
        MP_PASAJERO pasajero = new MP_PASAJERO();

        static public List<BE.PASAJERO> ListaPasajeros()
        {
            return MP_PASAJERO.ListarPasajeros();
        }

        public void AltaPasajero(BE.PASAJERO p)
        {
            pasajero.AltaPasajero(p);
        }

        public void ModificarPasajero(BE.PASAJERO p)
        {
            pasajero.ModificarPasajero(p);
        }

        public void BajaPasajero(BE.PASAJERO p)
        {
            pasajero.BajaPasajero(p);
        }


    }
}
